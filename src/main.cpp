#include <AntTweakBar.h>
#include "gl_core_4_2.h"
#include "utility.h"
#include "gl_util.h"
#include "objOpenGL.h"

#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include <GL/freeglut.h>

#include <cmath>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>




#define BUFFER_OFFSET(i) ((char *)nullptr + (i))

namespace
{
	std::string const applicationName("BTF-Texturing");
	
	std::string const btfVertShaderSource("data/shader/btf.vs");
	std::string const btfFragShaderSource("data/shader/btf.fs");
	std::string const lightVertShaderSource("data/shader/light.vs");
	std::string const lightFragShaderSource("data/shader/light.fs");
	std::string const coordSysVertShaderSource("data/shader/coordSys.vs");
	std::string const coordSysFragShaderSource("data/shader/coordSys.fs");
	
	int windowWidth(1520);
	int viewportWidth = windowWidth-240;
	int windowHeight(720);
	float nearPlane = 1.0f;
	float farPlane = 1000.0f;
	glm::vec4 backgroundColor(0.705f, 0.705f, 0.705f, 1.0f);
	
	int const openglMajorVersion(4);
	int const openglMinorVersion(2);
	
	glm::mat4 modelMatrix (1.0f);
	glm::mat4 viewMatrix (1.0f);
	glm::mat4 projectionMatrix (1.0f);
	glm::mat4 modelMatrixLight (1.0f);
	
	int lastButton;
	glm::vec3 lastMousePos;
	bool switchMouseFunc = true;
	bool switchLightMoveMode = true;
	bool switchRotZoomMode = true;
	bool showLightBulb = true;
	bool showCoordSys = true;
	long long screenshotCounter = 0;
		
	GLuint btfProgramID;
	GLuint lightProgramID;
	GLuint coordSysProgramID;
	
	GLuint vaoLightID;
	GLuint vboLightID;
	GLuint vaoCoordSysID;
	GLuint vboCoordSysID;
	std::vector<std::vector<GLuint> > texID (2);
	
	ObjOpenGL obj;
	std::string objPath ("data/obj/");
	char objFile[64] = "couch.obj";
	int objCount = 1;
	
	bool const compress = true;
	std::string textureListFile ("list.txt");
	std::string texturePath ("data/textures/");
//	int const texSize = 256;
//	int const logTexSize = int(std::log10(float(texSize))/std::log10(2.0f));
	char texObject[2][64] = {"cord", "cord"};
	int texCounter = 0;
//	if theta interval != 15 -> modify shader
	float phiInterval[6] = {360.0f, 60.0f, 30.0f, 20.0f, 18.0f, 15.0f};
	float phiIntervalCount[7] = {0.0f, 1.0f, 7.0f, 19.0f, 37.0f, 57.0f, 81.0f};
	unsigned int texPerViewDir = 81;
	float thetaInterval = 15.0f;
	float textureScalelo = 35.0f;
	float textureScalero = 35.0f;
	bool mipmappinglo = true;
	bool mipmappingro = true;
	GLuint samplers[2] = {0};
	int maxTexLayers = 0;	
	
	
	TwBar* gui;
	typedef enum { X64 = 64, X128 = 128, X256 = 256, X512 = 512} TextureSize;
	TwEnumVal tsEV[] = { {X64, "64"}, {X128, "128"}, {X256, "256"}, {X512, "512"} };
	TextureSize texSize[2] = {X256, X256};
	
	typedef enum { JPG = 0, RAW = 1, BMP = 2 } ImageType;
	TwEnumVal itEV[] = { {JPG, "*.jpg"}, {RAW, "*.raw"}, {BMP, "*.bmp"} };
	ImageType imgType[2] = {JPG, JPG};
	
	typedef enum { FRST = 1, SCND, TRD, FRTH, FFTH, SXTH} TextureFrequ;
	TwEnumVal tEV[] = { {FRST, "1."}, {SCND, "2."}, {TRD, "3."}, {FRTH, "4."}, {FFTH, "5."}, {SXTH, "6."} };
	TextureFrequ thetalight1 = FRST;
	TextureFrequ philight1   = FRST;
	TextureFrequ thetalight2 = FRST;
	TextureFrequ philight2   = FRST;
	TextureFrequ thetaview1 = FRST;
	TextureFrequ phiview1   = FRST;
	TextureFrequ thetaview2 = FRST;
	TextureFrequ phiview2   = FRST;
	
	glm::vec4 rotation(0.0f, 0.0f, 0.0f, 1.0f);
	float zoom = -4.0;
	glm::vec3 lightPosition (0.0f, 0.0f, 0.0f);

} //namespace

bool initDebugOutput();
bool initProgram(GLuint& program, std::string const& vertShaderSource, std::string const& fragShaderSource);
bool initLight();
bool initCoordSys();
bool initTexture();
bool initSamplers();
bool initMatrices();
bool initObj();
bool initAntTweakBar();
void setTexUniforms();
void setMatrixUniforms();
void setUniforms();
void reload(void*);
void display();
bool cleanupGL();
void key(unsigned char key, int x, int y);
void specialKey(int key, int x, int y);
void motionFunc(int x, int y);
void mouseFunc(int button, int state, int x, int y);
void resize(int width, int height);
bool initGL();
bool initGLUT (int argc, char* argv[]);



bool initDebugOutput()
{
	bool validated = true;

	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	glDebugMessageControlARB(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	glDebugMessageCallbackARB(debugOutput, nullptr);

	return validated;
}


bool initProgram(GLuint& program, std::string const& vertShaderSource, std::string const& fragShaderSource)
{
	bool validated = true;

	{
		GLuint vsShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fsShader = glCreateShader(GL_FRAGMENT_SHADER);
	
		std::ifstream vsFile (vertShaderSource);
		std::string vsSource ((std::istreambuf_iterator<char>(vsFile)), std::istreambuf_iterator<char>());
		const char* vsSourceTemp = vsSource.c_str();
		glShaderSource(vsShader, 1, &vsSourceTemp, 0);
		glCompileShader (vsShader);
		validated = validated && printShaderInfoLog(vsShader);
		
		std::ifstream fsFile (fragShaderSource);
		std::string fsSource ((std::istreambuf_iterator<char>(fsFile)), std::istreambuf_iterator<char>());
		const char* fsSourceTemp = fsSource.c_str();
		glShaderSource(fsShader, 1, &fsSourceTemp, 0);
		glCompileShader (fsShader);
		validated = validated && printShaderInfoLog(fsShader);
		
		program = glCreateProgram();
		
		glAttachShader (program, vsShader);
		glAttachShader (program, fsShader);
	
		glLinkProgram (program);
		validated = validated && printProgramInfoLog(program);
		
		glDeleteShader(vsShader);
		glDeleteShader(fsShader);
	}

	return validated;
}


bool initLight()
{
	bool validated = true;
	
	glm::vec4 pl(0.0f, 0.0f, 0.0f, 1.0f);
	glGenBuffers(1, &vboLightID);
		glBindBuffer(GL_ARRAY_BUFFER, vboLightID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4), glm::value_ptr(pl), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenVertexArrays(1, &vaoLightID);
	glBindVertexArray(vaoLightID);
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboLightID);
		
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
	}
	glBindVertexArray(0);

	return validated;
}


bool initCoordSys()
{
	bool validated = true;

	std::vector<glm::vec3> kos;
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(1.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 0.0f));
	kos.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	
	glGenBuffers(1, &vboCoordSysID);
		glBindBuffer(GL_ARRAY_BUFFER, vboCoordSysID);
		glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(glm::vec4), kos.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glGenVertexArrays(1, &vaoCoordSysID);
	glBindVertexArray(vaoCoordSysID);
	{
		glBindBuffer(GL_ARRAY_BUFFER, vboCoordSysID);
		
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
	}
	glBindVertexArray(0);

	return validated;
}


bool initTexture(unsigned int objID)
{
	bool validated = true;

	std::string path (texturePath + std::to_string(static_cast<long long>(texSize[objID])) + 'x' + std::to_string(static_cast<long long>(texSize[objID])) + '/' + texObject[objID] + '/');
	
	std::cout << "Initializing BTF-data for \"" << texObject[objID] << "\", size: " << texSize[objID] << "x" << texSize[objID] << " (Object " << objID << ")" << std::endl;
	std::ifstream in(texturePath + textureListFile, std::ios::in);
	if (!in)
	{
		std::cerr << "Cannot open " << texturePath + textureListFile << std::endl;
		validated = false;
		return validated;
	}
	
	std::string line;
	int counter = 0;
	while (getline(in, line)) 
	{
		++counter;
	}
	in.clear();
	in.seekg(0, std::ifstream::beg);
	
	
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxTexLayers);
	int arraySize = static_cast<int>(std::ceil(float(counter)/2048.0f));
	if (maxTexLayers < 2048)
	{
		std::cerr << "Number of texture layers to high for this graphic card.\nmax number: " << maxTexLayers << "\nrequested number: " << arraySize << "x2048" << std::endl;
		return false;
	}
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	texID[objID].resize(arraySize);
	glGenTextures(arraySize, &texID[objID][0]);
	std::cout << "Allocating texture storage...";
	for (unsigned int i = 0; i < texID[objID].size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0+i);
		glBindTexture(GL_TEXTURE_2D_ARRAY, texID[objID][i]);
		{
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			if (compress)
			{
				glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, texSize[objID], texSize[objID], 2048, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
			}
			else
			{
				glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, texSize[objID], texSize[objID], 2048, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
			}
		}
	}
	std::cout << "...done" << std::endl;

	counter = 0;
	int counterTa = 0;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, texID[objID][0]);
	std::cout << "Loading texture data";
	while (getline(in, line)) 
	{
		std::stringstream sstr (line);
		
		std::vector<char> image;
		if (imgType[objID] == JPG)
		{
			image = readJpegImage(path + std::move(line));
		}
		else if (imgType[objID] == RAW)
		{
			image = readRawImage(path + std::move(line));
		}
		else if (imgType[objID] == BMP)
		{
			image = readBmpImage(path + std::move(line));
		}
		else
		{
			std::cerr << "\nerror: invalid image type, cannot read textures" << std::endl;
			return false;
		}
		
		if (image.size() == 0)
		{
			return false;
		}
		
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, counter, texSize[objID], texSize[objID], 1, GL_RGB, GL_UNSIGNED_BYTE, image.data());
		++counter;
		
		if (counter == 2048)
		{
			counter = 0;
			++counterTa;
			glActiveTexture(GL_TEXTURE0 + counterTa);
			glBindTexture(GL_TEXTURE_2D_ARRAY, texID[objID][counterTa]);
		}
		if (counter % 100 == 0)
		{
			std::cout << "." << std::flush;
		}		
	}
	std::cout << "done" << std::endl;
	
	std::cout << "Generating mipmaps";
	for (unsigned int i = 0; i < texID[objID].size(); ++i)
	{
		std::cout << "." << std::flush;
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D_ARRAY, texID[objID][i]);
		glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
	}
	std::cout << "done\n" << "Initialization of BTF-data complete." << std::endl;
	
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	validated = validated && printProgramInfoLog(btfProgramID);
	
	return validated;
}

bool initSamplers()
{
	bool validated = true;
	
	glGenSamplers(2, &samplers[0]);
	glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	
	return validated;
}

bool initMatrices()
{
	bool validated = true;
	
	modelMatrix = glm::mat4(1.0f);
	viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 3.0f), 
	                         glm::vec3(0.0f, 0.0f, 0.0f), 
	                         glm::vec3(0.0f, 1.0f, 0.0f));
	
	projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);
	modelMatrixLight = glm::translate(glm::mat4(1.0f), glm::vec3(5.0f, 3.0f, 0.0f));
	
	setMatrixUniforms();
			
	return validated;
}


bool initObj()
{
	bool validated = true;

	validated = obj.loadObj(objPath+objFile, true);
	
	return validated;
}


void TW_CALL saveImage(void*)
{
	std::string filename("screenshot");
	
	writeCurrentFrameToBMP(std::string(filename + std::to_string(screenshotCounter) + std::string(".bmp")), 240, 0, windowWidth-240, windowHeight);
	std::cout << "written current frame to " << filename << screenshotCounter << ".bmp" << std::endl;
	++screenshotCounter;
}
			
void TW_CALL reloadTex1(void*)
{
	if(texID[0].size() > 0)
	{
		glDeleteTextures(texID[0].size(), &texID[0][0]);
	}

	bool validated = true;
	
	validated = initTexture(0);

	if (validated == false)
	{
		std::cerr << "error: unable to load new texture data" << std::endl;
	}
}

void TW_CALL reloadTex2(void*)
{
	if(texID[1].size() > 0)
	{
		glDeleteTextures(texID[1].size(), &texID[1][0]);
	}

	bool validated = true;
	
	validated = initTexture(1);

	if (validated == false)
	{
		std::cerr << "error: unable to load new texture data" << std::endl;
	}
}

void TW_CALL reloadObj(void*)
{	
	bool validated = true;
	
	validated = initObj();

	if (validated == false)
	{
		std::cerr << "error: unable to load new obj" << std::endl;
	}
}

void TW_CALL setRotation(const void* newVal, void *)
{
	rotation = *(const glm::vec4*) newVal;
	
	setMatrixUniforms();
}
void TW_CALL getRotation(void* val, void *) {*(glm::vec4*) val = rotation; }

void TW_CALL setZoom(const void* newVal, void *)
{
	zoom = *(const float*) newVal;
	
	setMatrixUniforms();
}
void TW_CALL getZoom(void* val, void *) {*(float*) val = zoom; }

void TW_CALL setLightPosX(const void* newVal, void *)
{
	lightPosition.x = *(const float*) newVal;
	
	setMatrixUniforms();
}
void TW_CALL getLightPosX(void* val, void *) {*(float*) val = lightPosition.x; }
void TW_CALL setLightPosY(const void* newVal, void *)
{
	lightPosition.y = *(const float*) newVal;
	
	setMatrixUniforms();
}
void TW_CALL getLightPosY(void* val, void *) {*(float*) val = lightPosition.y; }
void TW_CALL setLightPosZ(const void* newVal, void *)
{
	lightPosition.z = *(const float*) newVal;
	
	setMatrixUniforms();
}
void TW_CALL getLightPosZ(void* val, void *) {*(float*) val = lightPosition.z; }


void TW_CALL setThetaLight1(const void* newVal, void *)
{
	thetalight1 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "thetaLight1");
	glProgramUniform1f (btfProgramID, loc, float(thetalight1));
}
void TW_CALL getThetaLight1(void* val, void *) {*(TextureFrequ*) val = thetalight1; }

void TW_CALL setPhiLight1(const void* newVal, void *)
{
	philight1 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "phiLight1");
	glProgramUniform1f (btfProgramID, loc, float(philight1));
}
void TW_CALL getPhiLight1(void* val, void *) {*(TextureFrequ*) val = philight1; }

void TW_CALL setThetaLight2(const void* newVal, void *)
{
	thetalight2 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "thetaLight2");
	glProgramUniform1f (btfProgramID, loc, float(thetalight2));
}
void TW_CALL getThetaLight2(void* val, void *) {*(TextureFrequ*) val = thetalight2; }

void TW_CALL setPhiLight2(const void* newVal, void *)
{
	philight2 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "phiLight2");
	glProgramUniform1f (btfProgramID, loc, float(philight2));
}
void TW_CALL getPhiLight2(void* val, void *) {*(TextureFrequ*) val = philight2; }

void TW_CALL setThetaView1(const void* newVal, void *)
{
	thetaview1 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "thetaView1");
	glProgramUniform1f (btfProgramID, loc, float(thetaview1));
}
void TW_CALL getThetaView1(void* val, void *) {*(TextureFrequ*) val = thetaview1; }

void TW_CALL setPhiView1(const void* newVal, void *)
{
	phiview1 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "phiView1");
	glProgramUniform1f (btfProgramID, loc, float(phiview1));
}
void TW_CALL getPhiView1(void* val, void *) {*(TextureFrequ*) val = phiview1; }

void TW_CALL setThetaView2(const void* newVal, void *)
{
	thetaview2 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "thetaView2");
	glProgramUniform1f (btfProgramID, loc, float(thetaview2));
}
void TW_CALL getThetaView2(void* val, void *) {*(TextureFrequ*) val = thetaview2; }

void TW_CALL setPhiView2(const void* newVal, void *)
{
	phiview2 = *(const TextureFrequ*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "phiView2");
	glProgramUniform1f (btfProgramID, loc, float(phiview2));
}
void TW_CALL getPhiView2(void* val, void *) {*(TextureFrequ*) val = phiview2; }

void TW_CALL setTexScale1(const void* newVal, void *)
{
	textureScalelo = *(const float*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "tcm1");
	glProgramUniform1f (btfProgramID, loc, float(textureScalelo));
}
void TW_CALL getTexScale1(void* val, void *) {*(float*) val = textureScalelo; }

void TW_CALL setTexScale2(const void* newVal, void *)
{
	textureScalero = *(const float*) newVal;
	
	GLuint loc = glGetUniformLocation (btfProgramID, "tcm2");
	glProgramUniform1f (btfProgramID, loc, float(textureScalero));
}
void TW_CALL getTexScale2(void* val, void *) {*(float*) val = textureScalero; }

void TW_CALL setEnableMipmaps1(const void* newVal, void *)
{
	mipmappinglo = *(const bool*) newVal;
	
	if (mipmappinglo == true)
	{
		glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glSamplerParameteri(samplers[0], GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
}
void TW_CALL getEnableMipmaps1(void* val, void *) {*(bool*) val = mipmappinglo; }

void TW_CALL setEnableMipmaps2(const void* newVal, void *)
{
	mipmappingro = *(const bool*) newVal;

	if (mipmappingro == true)
	{
		glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	}
	else
	{
		glSamplerParameteri(samplers[1], GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	}
}
void TW_CALL getEnableMipmaps2(void* val, void *) {*(bool*) val = mipmappingro; }

void TW_CALL setObjCount(const void* newVal, void *)
{
	bool validated = true;
	if (objCount == *(const int*) newVal)
	{
		return;
	}
	objCount = *(const int*) newVal;
	viewportWidth = (windowWidth-240)/objCount;
	
	projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);
	setMatrixUniforms();
	
	if (objCount == 1)
	{
		validated = TwRemoveVar(gui, "emptyline4");
		validated = TwRemoveVar(gui, "right object:");
		validated = TwRemoveVar(gui, "texturero");
		validated = TwRemoveVar(gui, "texturesizero");
		validated = TwRemoveVar(gui, "texturetypero");
		validated = TwRemoveVar(gui, "Loadtexturero");
		validated = TwRemoveVar(gui, "emptyline5");
		validated = TwRemoveVar(gui, "rotexscale");
		validated = TwRemoveVar(gui, "romipmap");
		validated = TwRemoveVar(gui, "rotle");
		validated = TwRemoveVar(gui, "rople");
		validated = TwRemoveVar(gui, "rotve");
		validated = TwRemoveVar(gui, "ropve");
	}
	else //if(objCount == 2)
	{
		TwType tst = TwDefineEnum("Texturesize", tsEV, 4);
		TwType tdt = TwDefineEnum("Texturedatatype", itEV, 3);
		TwType tle = TwDefineEnum("ThetaLightEach", tEV, 3);
		TwType ple = TwDefineEnum("PhiLightEach", tEV, 6);
		TwType tve = TwDefineEnum("ThetaViewEach", tEV, 3);
		TwType pve = TwDefineEnum("PhiViewEach", tEV, 6);
	
		validated = TwAddButton(gui, "emptyline4", nullptr, nullptr, " label=' ' ");
	
		validated = TwAddButton(gui, "right object:", nullptr, nullptr, nullptr);

		validated = TwAddVarRW(gui, "texturero", TW_TYPE_CSSTRING(sizeof(texObject[1])), texObject[1], " label='texture' ");

		validated = TwAddVarRW(gui, "texturesizero", tst, &texSize[1], " label='texture size' ");
	
		validated = TwAddVarRW(gui, "texturetypero", tdt, &imgType[1], " label='texture type' ");

		validated = TwAddButton(gui, "Loadtexturero", reloadTex2, nullptr, " label=' == Load Texture == ' ");
	
		validated = TwAddButton(gui, "emptyline5", nullptr, nullptr, " label=' ' ");		
		validated = TwAddVarCB(gui, "rotexscale", TW_TYPE_FLOAT, setTexScale2, getTexScale2, nullptr, " min=0.0 label='scale texture' ");
		
		validated = TwAddVarCB(gui, "romipmap", TW_TYPE_BOOLCPP, setEnableMipmaps2, getEnableMipmaps2, nullptr, " label='enable mipmaps' ");
		
		validated = TwAddVarCB(gui, "rotle", tle, setThetaLight2, getThetaLight2, nullptr, " label='theta light, each' ");
	
		validated = TwAddVarCB(gui, "rople", ple, setPhiLight2, getPhiLight2, nullptr,  " label='phi light, each' ");
	
		validated = TwAddVarCB(gui, "rotve", tve, setThetaView2, getThetaView2, nullptr, " label='theta View, each' ");
	
		validated = TwAddVarCB(gui, "ropve", pve, setPhiView2, getPhiView2, nullptr,  " label='phi View, each' ");	
	}

	if (validated == false)
	{
		std::cerr << "error: unable to change number of objs" << std::endl;
	}

}
void TW_CALL getObjCount(void* val, void *) {*(int*) val = objCount; }

bool initAntTweakBar()
{
	int validated = 1;
	
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(windowWidth, windowHeight);
	gui = TwNewBar("gui");
	TwDefine(" gui position='0 0' size='240 720' resizable=false ");
	
	
	TwType tst = TwDefineEnum("Texturesize", tsEV, 4);
	TwType tdt = TwDefineEnum("Texturedatatype", itEV, 3);
	TwType tle = TwDefineEnum("ThetaLightEach", tEV, 3);
	TwType ple = TwDefineEnum("PhiLightEach", tEV, 6);
	TwType tve = TwDefineEnum("ThetaViewEach", tEV, 3);
	TwType pve = TwDefineEnum("PhiViewEach", tEV, 6);
	
	
	validated = TwAddButton(gui, "save image", saveImage, nullptr, " label=' == Save Image == ' ");
	
	validated = TwAddButton(gui, "emptyline0", nullptr, nullptr, " label=' ' ");
	
	validated = TwAddVarRW(gui, "obj", TW_TYPE_CSSTRING(sizeof(objFile)), objFile, nullptr);
	
	validated = TwAddVarCB(gui, "obj count", TW_TYPE_INT32, setObjCount, getObjCount, nullptr, " min=1 max=2 ");
		
	validated = TwAddButton(gui, " == Load OBJ == ", reloadObj, nullptr, nullptr);
	
	validated = TwAddButton(gui, "emptyline1", nullptr, nullptr, " label=' ' ");
	

	validated = TwAddVarRW(gui, "background", TW_TYPE_COLOR4F, glm::value_ptr(backgroundColor), nullptr);
	
	validated = TwAddButton(gui, "emptyline1b", nullptr, nullptr, " label=' ' ");
	
	validated = TwAddVarCB(gui, "rotation", TW_TYPE_QUAT4F, setRotation, getRotation, nullptr, "showval=true");
	
	validated = TwAddVarCB(gui, "zoom", TW_TYPE_FLOAT, setZoom, getZoom, nullptr, " step=0.10 ");
	
	validated = TwAddButton(gui, "emptyline1c", nullptr, nullptr, " label=' ' ");
	
	validated = TwAddButton(gui, "light position", nullptr, nullptr, nullptr);
	
	validated = TwAddVarCB(gui, "light position x", TW_TYPE_FLOAT, setLightPosX, getLightPosX, nullptr, " label='x' step=0.10 ");
	validated = TwAddVarCB(gui, "light position y", TW_TYPE_FLOAT, setLightPosY, getLightPosY, nullptr, " label='y' step=0.10 ");
	validated = TwAddVarCB(gui, "light position z", TW_TYPE_FLOAT, setLightPosZ, getLightPosZ, nullptr, " label='z' step=0.10 ");
	
	validated = TwAddButton(gui, "emptyline1d", nullptr, nullptr, " label=' ' ");
	
	validated = TwAddVarRW(gui, "display coord-sys", TW_TYPE_BOOLCPP, &showCoordSys, nullptr);
	validated = TwAddVarRW(gui, "display light bulb", TW_TYPE_BOOLCPP, &showLightBulb, nullptr);
	
	
	validated = TwAddButton(gui, "emptyline2", nullptr, nullptr, " label=' ' ");
	
	validated = TwAddButton(gui, "left object:", nullptr, nullptr, nullptr);
	
	validated = TwAddVarRW(gui, "texture", TW_TYPE_CSSTRING(sizeof(texObject[0])), texObject[0], nullptr);

	validated = TwAddVarRW(gui, "texture size", tst, &texSize[0], nullptr);
	
	validated = TwAddVarRW(gui, "texture type", tdt, &imgType[0], nullptr);

	validated = TwAddButton(gui, " == Load Texture == ", reloadTex1, nullptr, nullptr);
	
	validated = TwAddButton(gui, "emptyline3", nullptr, nullptr, " label=' ' ");
	
	
	validated = TwAddVarCB(gui, "lotexscale", TW_TYPE_FLOAT, setTexScale1, getTexScale1, nullptr, " min=0.0 label='scale texture' ");
	
	validated = TwAddVarCB(gui, "lomipmap", TW_TYPE_BOOLCPP, setEnableMipmaps1, getEnableMipmaps1, nullptr, " label='enable mipmaps' ");
			
	validated = TwAddVarCB(gui, "lotle", tle, setThetaLight1, getThetaLight1, nullptr, " label='theta light, each' ");
	
	validated = TwAddVarCB(gui, "lople", ple, setPhiLight1, getPhiLight1, nullptr, " label='phi light, each' ");
	
	validated = TwAddVarCB(gui, "lotve", tve, setThetaView1, getThetaView1, nullptr, " label='theta View, each' ");
	
	validated = TwAddVarCB(gui, "lopve", pve, setPhiView1, getPhiView1, nullptr, " label='phi View, each' ");
	
	
	return validated == 1;
}


void setTexUniforms(std::vector<GLuint> const& tex)
{
	for (unsigned int i = 0; i < tex.size(); ++i)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D_ARRAY, tex[i]);
		std::string t = std::string("texArray[") + std::to_string(static_cast<long long>(i)) + std::string("]");
		GLuint tLoc = glGetUniformLocation (btfProgramID, t.c_str());
		glProgramUniform1i (btfProgramID, tLoc, i);	
	}
	
	GLuint tsLoc = glGetUniformLocation (btfProgramID, "texArraySize");
	glProgramUniform1i (btfProgramID, tsLoc, tex.size());
	
	GLuint tcLoc = glGetUniformLocation (btfProgramID, "texCounter");
	glProgramUniform1i (btfProgramID, tcLoc, texCounter);
}


void setMatrixUniforms()
{
	glm::mat4 vm, mm, ml;
	if (switchLightMoveMode)
	{
		ml = glm::translate(glm::mat4(1.0f), lightPosition);
	}
	else
	{
		ml = modelMatrixLight;
	}
	
	if (switchRotZoomMode)
	{
		float yy2 = 2.0f * rotation[1] * rotation[1];
		float xy2 = 2.0f * rotation[0] * rotation[1];
		float xz2 = 2.0f * rotation[0] * rotation[2];
		float yz2 = 2.0f * rotation[1] * rotation[2];
		float zz2 = 2.0f * rotation[2] * rotation[2];
		float wz2 = 2.0f * rotation[3] * rotation[2];
		float wy2 = 2.0f * rotation[3] * rotation[1];
		float wx2 = 2.0f * rotation[3] * rotation[0];
		float xx2 = 2.0f * rotation[0] * rotation[0];
		mm[0][0] = - yy2 - zz2 + 1.0f;
		mm[0][1] = xy2 + wz2;
		mm[0][2] = xz2 - wy2;
		mm[0][3] = 0.0f;
		
		mm[1][0] = xy2 - wz2;
		mm[1][1] = - xx2 - zz2 + 1.0f;
		mm[1][2] = yz2 + wx2;
		mm[1][3] = 0.0f;
		
		mm[2][0] = xz2 + wy2;
		mm[2][1] = yz2 - wx2;
		mm[2][2] = - xx2 - yy2 + 1.0f;
		mm[2][3] = 0.0f;
		
		mm[3][0] = mm[3][1] = mm[3][2] = 0.0f;
		mm[3][3] = 1.0f;

		vm = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, zoom));
	}
	else
	{
		mm = modelMatrix;
		vm = viewMatrix;
	}

	//obj
	glm::mat4 mvMatrix = vm * mm;
	GLuint mvLoc = glGetUniformLocation (btfProgramID, "mv");
	glProgramUniformMatrix4fv (btfProgramID, mvLoc, 1, GL_FALSE, glm::value_ptr(mvMatrix));
	
	glm::mat4 mvpMatrix = projectionMatrix * vm * mm;
	GLuint mvpLoc = glGetUniformLocation (btfProgramID, "mvp");
	glProgramUniformMatrix4fv (btfProgramID, mvpLoc, 1, GL_FALSE, glm::value_ptr(mvpMatrix));

	//light
	glm::mat4 vplMatrix = vm * ml;
	GLuint vplLoc = glGetUniformLocation (lightProgramID, "vp");
	glProgramUniformMatrix4fv (lightProgramID, vplLoc, 1, GL_FALSE, glm::value_ptr(vplMatrix));
		
	glm::mat4 mvplMatrix = projectionMatrix * vm * ml;
	GLuint mvplLoc = glGetUniformLocation (lightProgramID, "mvp");
	glProgramUniformMatrix4fv (lightProgramID, mvplLoc, 1, GL_FALSE, glm::value_ptr(mvplMatrix));
	
	glm::vec3 pl = glm::vec3(vm * ml * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
	GLuint lLoc = glGetUniformLocation (btfProgramID, "posLightEye");
	glProgramUniform3fv (btfProgramID, lLoc, 1, glm::value_ptr(pl));
	
	//coordsys
	glm::mat4 vpcMatrix = projectionMatrix * vm;
	GLuint vpcLoc = glGetUniformLocation (coordSysProgramID, "vp");
	glProgramUniformMatrix4fv (coordSysProgramID, vpcLoc, 1, GL_FALSE, glm::value_ptr(vpcMatrix));
}


void setUniforms()
{
	for (unsigned int i = 0; i < texID.size(); ++i)
	{
		setTexUniforms(texID[i]);
	}
	
	setMatrixUniforms();
	
			
	GLuint tpiLoc = glGetUniformLocation (btfProgramID, "phiInterval");
	glProgramUniform1fv (btfProgramID, tpiLoc, sizeof(phiInterval)/sizeof(float), phiInterval);
	
	GLuint tpicLoc = glGetUniformLocation (btfProgramID, "phiIntervalCount");
	glProgramUniform1fv (btfProgramID, tpicLoc, sizeof(phiIntervalCount)/sizeof(float), phiIntervalCount);
	
	GLuint ttpvdLoc = glGetUniformLocation (btfProgramID, "texPerViewDir");
	glProgramUniform1ui (btfProgramID, ttpvdLoc, texPerViewDir);
	
	GLuint ttiLoc = glGetUniformLocation (btfProgramID, "thetaInterval");
	glProgramUniform1f (btfProgramID, ttiLoc, thetaInterval);
}


void display()
{
	glClearColor(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	
	for (int i = 0; i < objCount; ++i)
	{
		setTexUniforms(texID[i]);
		
		for (unsigned int j = 0; j < texID[i].size(); ++j)
		{
			glBindSampler(j, samplers[i]);
		}

		glViewport(i * viewportWidth + 240, 0, viewportWidth, windowHeight);
	
		glUseProgram(btfProgramID);
		GLuint oloc1 = glGetUniformLocation (btfProgramID, "objectID");
		glUniform1i (oloc1, i);
			obj.draw();


		if (showLightBulb)
		{
			glBindVertexArray(vaoLightID);
			glUseProgram(lightProgramID);
				glDrawArrays(GL_POINTS, 0, 1);	
		}
		
		if (showCoordSys)
		{
			glBindVertexArray(vaoCoordSysID);
			glUseProgram(coordSysProgramID);
				glDrawArrays(GL_LINES, 0, 6);	
		}
		
		for (unsigned int j = 0; j < texID[i].size(); ++j)
		{
			glBindSampler(j, 0);
		}
	}
	
	glViewport(0, 0, windowWidth, windowHeight);
	
	TwDraw();
	glutSwapBuffers();
}


bool cleanupGL()
{
	bool validated = true;

	glDeleteProgram(btfProgramID);
	glDeleteProgram(lightProgramID);
	glDeleteProgram(coordSysProgramID);
	
	for (unsigned int i = 0; i < texID.size(); ++i)
	{
		if(texID[i].size() > 0)
		{
			glDeleteTextures(texID[i].size(), &texID[i][0]);
		}	
	}
	glDeleteSamplers(2, &samplers[0]);
	
	glDeleteVertexArrays(1, &vaoLightID);
	glDeleteBuffers(1, &vboLightID);
	glDeleteVertexArrays(1, &vaoCoordSysID);
	glDeleteBuffers(1, &vboCoordSysID);

	return validated;
}


void key(unsigned char key, int x, int y)
{
	GLuint tcLoc;
	
	TwEventKeyboardGLUT(key, x, y);
	switch (key)
	{
		/// press ESC or q to quit
		case 27 :
		case 'q':
			cleanupGL();
			exit(0);
			break;
		case 't':
			tcLoc = glGetUniformLocation (btfProgramID, "texCounter");
			texCounter += 10;
			
			glProgramUniform1i (btfProgramID, tcLoc, texCounter);
			std::cout << texCounter << std::endl;
			break;
		case 's':
			initProgram(btfProgramID, btfVertShaderSource, btfFragShaderSource);
			initProgram(lightProgramID, lightVertShaderSource, lightFragShaderSource);
			initProgram(coordSysProgramID, coordSysVertShaderSource, coordSysFragShaderSource);
			setUniforms();
			
			break;
		case 'l':
			switchLightMoveMode = !switchLightMoveMode;
			setMatrixUniforms();
			break;
		case 'r':
			switchRotZoomMode = !switchRotZoomMode;
			setMatrixUniforms();
			break;
		case 'm':
			switchMouseFunc = !switchMouseFunc;
			break;
		case 'p':
			saveImage(nullptr);
			break;
		default:
			break;
	}

	glutPostRedisplay();
}

void specialKey(int key, int x, int y)
{

	switch (key)
	{
		case GLUT_KEY_LEFT :
			{
				viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3( 1.0f, 0.0f, 0.0f)) * viewMatrix;
				break;
			}
		case GLUT_KEY_RIGHT :
			{
				viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, 0.0f, 0.0f)) * viewMatrix;
				break;
			}
		case GLUT_KEY_UP :
			{
				viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3( 0.0f, 1.0f, 0.0f)) * viewMatrix;
				break;
			}
		case GLUT_KEY_DOWN :
			{
				viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3( 0.0f,-1.0f, 0.0f)) * viewMatrix;
				break;
			}
		default:
			{
				break;
		}
	}
	setMatrixUniforms();
	
	glutPostRedisplay();
}


void motionFunc(int x, int y)
{
	if (!TwEventMouseMotionGLUT(x, y))
	{
	if (lastButton == GLUT_LEFT_BUTTON)
	{
		glm::vec3 v(0.0f);
		float d;
		v.x = (2.0f * x - windowWidth) / float(windowWidth);
		v.y = (windowHeight - 2.0f * y) / float(windowHeight);
		// v.z = 0.0f;
		d = glm::length(v);
		d = d < 1.0f ? d : 1.0f;
		v.z = std::sqrt(1.001f - d*d);
		v = glm::normalize(v);

		glm::vec3 curPos = v;
		
		glm::vec3 dir = curPos - lastMousePos;
		float velo = glm::length(dir);
		if (velo > 0.0001f)
		{
			glm::vec3 rotAxis = glm::cross(lastMousePos, curPos);
			float rotAngle = velo * 50.0f;
			
			if (switchMouseFunc)
			{
				modelMatrix = glm::rotate(glm::mat4(1.0f), rotAngle, rotAxis) * modelMatrix;
			}
			else
			{
				viewMatrix = glm::rotate(glm::mat4(1.0f), rotAngle, rotAxis) * viewMatrix;
			}
		}
		lastMousePos = curPos;
	}
	else if (lastButton == GLUT_RIGHT_BUTTON)
	{
		glm::vec3 curPos (x, y, 0.0f);
		float yDiff = curPos.y - lastMousePos.y;
		viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, 0.1f*yDiff)) * viewMatrix;
			
		lastMousePos = curPos;
	}
	else if (lastButton == GLUT_MIDDLE_BUTTON)
	{
		glm::vec3 v(0.0f);
		float d;
		v.x = (2.0f * x - windowWidth) / float(windowWidth);
		v.y = (windowHeight - 2.0f * y) / float(windowHeight);
		// v.z = 0.0f;
		d = glm::length(v);
		d = d < 1.0f ? d : 1.0f;
		v.z = std::sqrt(1.001f - d*d);
		v = glm::normalize(v);

		glm::vec3 curPos = v;
		
		glm::vec3 dir = curPos - lastMousePos;
		float velo = glm::length(dir);
		if (velo > 0.0001f)
		{
			float rotAngle = velo * 50.0f;
			
			modelMatrixLight = glm::rotate(glm::mat4(1.0f), rotAngle, glm::vec3(0.0f, 1.0f, 0.0f)) * modelMatrixLight;
		}
		lastMousePos = curPos;
	}
	setMatrixUniforms();
	
	}	
	
	glutPostRedisplay();
}


void mouseFunc(int button, int state, int x, int y)
{
	if (!TwEventMouseButtonGLUT(button, state, x, y))
	{
		if (button == GLUT_LEFT_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			lastMousePos = v;
			lastButton = GLUT_LEFT_BUTTON;
		}
		else if (button == GLUT_RIGHT_BUTTON)
		{
			lastMousePos = glm::vec3(x, y, 0.0f);
			lastButton = GLUT_RIGHT_BUTTON;
		}
		else if (button == GLUT_MIDDLE_BUTTON)
		{
			glm::vec3 v(0.0f);
			float d;
			v.x = (2.0f * x - windowWidth) / float(windowWidth);
			v.y = (windowHeight - 2.0f * y) / float(windowHeight);
			// v.z = 0.0f;
			d = glm::length(v);
			d = d < 1.0f ? d : 1.0f;
			v.z = std::sqrt(1.001f - d*d);
			v = glm::normalize(v);

			lastMousePos = v;
			lastButton = GLUT_MIDDLE_BUTTON;
		}
		else
		{
			lastButton = button;
		}
	}
	glutPostRedisplay();
}


void resize(int width, int height)
{
	windowWidth = width;
	windowHeight = height;
	viewportWidth = (windowWidth-240)/objCount;

	glViewport(0, 0, windowWidth, windowHeight);
	projectionMatrix = glm::perspective(90.0f, float(viewportWidth) / float(windowHeight), nearPlane, farPlane);
	
	setMatrixUniforms();
	
	TwWindowSize(windowWidth, windowHeight);
	
	glutPostRedisplay();
}



bool initGL()
{
	bool validated = true;

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		validated = false;
	}
	
	if (validated)
	{
		validated = (ogl_IsVersionGEQ (4, 2) == 1);
	}
	if (validated)
	{
		validated = initDebugOutput();
	}
	if (validated)
	{
		validated = initProgram(btfProgramID, btfVertShaderSource, btfFragShaderSource);
		validated = validated && initProgram(lightProgramID, lightVertShaderSource, lightFragShaderSource);
		validated = validated && initProgram(coordSysProgramID, coordSysVertShaderSource, coordSysFragShaderSource);
	}
	if (validated)
	{
		validated = initLight();
	}
	if (validated)
	{
		validated = initCoordSys();
	}
	if (validated)
	{
//		validated = initTexture();
	}
	if (validated)
	{
		validated = initSamplers();
	}
	if (validated)
	{
		validated = initMatrices();
	}
	if (validated)
	{
		validated = initObj();
	}
	if (validated)
	{
		validated = initAntTweakBar();
	}
	
	if (validated)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_PROGRAM_POINT_SIZE);
		glLineWidth(5.0f);		
	}
	
	return validated;
}


bool initGLUT (int argc, char* argv[])
{
	bool validated = true;
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE | GLUT_DEPTH);
	
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(windowWidth, windowHeight);
	
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextFlags(GLUT_DEBUG);
	glutCreateWindow(argv[0]);
	
	glutReshapeFunc(resize);
	
	glutDisplayFunc(display);
	
	glutKeyboardFunc(key);
	glutSpecialFunc(specialKey);
	
	glutIdleFunc(nullptr);
	
	glutPassiveMotionFunc(nullptr);
	glutMotionFunc(motionFunc);
	glutMouseFunc(mouseFunc);
	
	return validated;
}


int main(int argc, char* argv[])
{
	bool result = true;
	
	initGLUT (argc, argv);
	
	result = initGL ();
	if (result)
	{
		glutMainLoop();
	}
	else
	{
		std::cerr << "Unable to initialize OpenGL, exiting..." << std::endl;
	}
	
	cleanupGL();
	
	return result;
}



