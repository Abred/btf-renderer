#version 420
//
uniform sampler2DArray texArray[10];
uniform int texArraySize;
uniform int texCounter;
uniform sampler2D tex;
//
uniform float phiInterval[6] = {360.0, 60.0, 30.0, 20.0, 18.0, 15.0};
uniform float phiIntervalCount[7] = {0.0, 1.0, 7.0, 19.0, 37.0, 57.0, 81.0};
uniform uint texPerViewDir = 81;
uniform float thetaInterval = 15.0;
uniform float thetaMaxAngle = 75.0;
uniform vec3 posLightEye = vec3 (75.0, 0.0, 0.0);
uniform float thetaLight1 = 1.0;
uniform float phiLight1 = 1.0;
uniform float thetaLight2 = 1.0;
uniform float phiLight2 = 1.0;
uniform float thetaView1 = 1.0;
uniform float phiView1 = 1.0;
uniform float thetaView2 = 1.0;
uniform float phiView2 = 1.0;
uniform float tcm1 = 35.0;
uniform float tcm2 = 35.0;
uniform int objectID = 0;
//
in vec4 vPosition;
in vec2 vTexCoord;
in vec3 vNormalEye;
in vec3 vTangentEye;
in vec3 vBiTangentEye;
//in mat3 tbn;
//
out vec4 fragColor;
//
const vec3 posViewEye = vec3 (0.0, 0.0, 0.0);

float thetaLightEach = 1.0;
float phiLightEach = 1.0;
float thetaViewEach = 1.0;
float phiViewEach = 1.0;
float tcm = 35.0;

vec4 mixPhiLight(in int thetaLightID, in float phiLight, in float texIDAccum)
{
	float phiLightID = phiLight / phiInterval[thetaLightID] / phiLightEach;
	float phiLightFl  = floor(phiLightID) * phiLightEach;
	float phiLightCe  = ceil (phiLightID) * phiLightEach;
	float phiLightMix = fract(phiLightID);
	
	float texIDAccumFl = texIDAccum + phiIntervalCount[thetaLightID] + phiLightFl;
	float texIDAccumCe = texIDAccum + phiIntervalCount[thetaLightID] + phiLightCe;
	
//	if (phiIntervalCount[thetaLightID] + phiLightCe >= phiIntervalCount[thetaLightID+1])
	if (phiLight + phiInterval[int(thetaLightID)] >= 360)
	{
		texIDAccumCe = texIDAccum + phiIntervalCount[thetaLightID];
	}

#ifdef __GLSL_CG_DATA_TYPES
	vec4 phiLightFlCol = texture(texArray[int(floor(texIDAccumFl/2048))], vec3(vTexCoord * tcm, mod(texIDAccumFl,2048)));
	vec4 phiLightCeCol = texture(texArray[int(floor(texIDAccumCe/2048))], vec3(vTexCoord * tcm, mod(texIDAccumCe,2048)));
#else
	vec4 fl[10];
	vec4 ce[10];
	for (uint i = 0; i < texArraySize; ++i)
	{
		fl[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumFl,2048)));
		ce[i] = texture(texArray[i], vec3(vTexCoord * tcm, mod(texIDAccumCe,2048)));
	}
	vec4 phiLightFlCol = fl[int(floor(texIDAccumFl/2048))];
	vec4 phiLightCeCol = ce[int(floor(texIDAccumCe/2048))];
#endif

	return mix(phiLightFlCol, phiLightCeCol, phiLightMix);
}

vec4 mixThetaLight(in float thetaLight, in float phiLight, in float texIDAccum)
{
	float thetaLightID = thetaLight / thetaInterval / thetaLightEach;
	float thetaLightFl  = floor(thetaLightID) * thetaLightEach;
	float thetaLightCe  = ceil (thetaLightID) * thetaLightEach;
	float thetaLightMix = fract(thetaLightID);

	vec4 thetaLightFlCol = mixPhiLight(int(thetaLightFl), phiLight, texIDAccum);
	vec4 thetaLightCeCol = mixPhiLight(int(thetaLightCe), phiLight, texIDAccum);
	
	// upper bound, if higher -> no light/shadow
	// e.g. theta = 80
	// =>   thetaFloor = 75 -- ok
	//      thetaCeil  = 90 !! not available, no light
	//  ==> thetaCeilColor  = black
	float maxThetaLightID = thetaMaxAngle/thetaInterval;
	if (thetaLightFl > maxThetaLightID)
	{
		thetaLightFlCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	if (thetaLightCe > maxThetaLightID)
	{
		thetaLightCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}	

	
	return mix(thetaLightFlCol, thetaLightCeCol, thetaLightMix);
}


vec4 mixPhiView(in int thetaViewID, in float phiView, in float thetaLight, in float phiLight)
{
	float phiViewID = phiView / phiInterval[thetaViewID] / phiViewEach;
	float phiViewFl  = floor(phiViewID) * phiViewEach;
	float phiViewCe  = ceil (phiViewID) * phiViewEach;
	float phiViewMix = fract(phiViewID);
	
	float texIDAccumFl = texPerViewDir * (phiIntervalCount[thetaViewID]+phiViewFl);
	float texIDAccumCe = texPerViewDir * (phiIntervalCount[thetaViewID]+phiViewCe);
	
	// upper bound, wrap around to phi = 0
	// e.g. phi = 345, interval = 60
	// =>   phiFloor = 300
	//      phiCeil  = 360 !! 360 not available, but 360 == 0
	//  ==> phiCeil  = 0
//	if (phiIntervalCount[thetaViewID] + phiViewCe >= phiIntervalCount[thetaViewID+1])
	if (phiView + phiInterval[int(thetaViewID)] >= 360)
	{
		texIDAccumCe = texPerViewDir * phiIntervalCount[thetaViewID];
	}
	
	vec4 phiViewFlCol = mixThetaLight(thetaLight, phiLight, texIDAccumFl);
	vec4 phiViewCeCol = mixThetaLight(thetaLight, phiLight, texIDAccumCe);
	
	return mix(phiViewFlCol, phiViewCeCol, phiViewMix);	
}


vec4 calcColor(in float thetaView, in float phiView, in float thetaLight, in float phiLight)
{
	// nearest lower angle, nearest higher angle and interpolation factor
	float thetaViewID = thetaView / thetaInterval / thetaViewEach;
	float thetaViewFl  = floor(thetaViewID) * thetaViewEach;
	float thetaViewCe  = ceil (thetaViewID) * thetaViewEach;
	float thetaViewMix = fract(thetaViewID);
	
	// upper bound, clamp to highest available theta
	// e.g. theta = 80
	// =>   thetaFloor = 75
	//      thetaCeil  = 90 !! not available 
	//  ==> thetaCeil  = 75
	float maxThetaViewID = thetaMaxAngle/thetaInterval;
	if (thetaViewFl > maxThetaViewID)
	{
		thetaViewFl = maxThetaViewID;
//		thetaViewCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	if (thetaViewCe > maxThetaViewID)
	{
		thetaViewCe = maxThetaViewID;
//		thetaViewCeCol = vec4(0.0, 0.0, 0.0, 1.0);
	}
	
	vec4 thetaViewFlCol = mixPhiView(int(thetaViewFl), phiView, thetaLight, phiLight);
	vec4 thetaViewCeCol = mixPhiView(int(thetaViewCe), phiView, thetaLight, phiLight);	
	
	return mix(thetaViewFlCol, thetaViewCeCol, thetaViewMix);
}


void main()
{
	if (objectID == 0)
	{
		thetaLightEach = thetaLight1;
		phiLightEach = phiLight1;
		thetaViewEach = thetaView1;
		phiViewEach = phiView1;
		tcm = tcm1;
	}
	else
	{
		thetaLightEach = thetaLight2;
		phiLightEach = phiLight2;
		thetaViewEach = thetaView2;
		phiViewEach = phiView2;
		tcm = tcm2;
	}
	
	// transformation to tangent space
	mat3 tbn = inverse(mat3(normalize(vTangentEye), normalize(vBiTangentEye), normalize(vNormalEye)));
	
	vec3 dirLightTangent = tbn * (posLightEye - vPosition.xyz);
	vec3 dirViewTangent = tbn * (posViewEye - vPosition.xyz);
	vec3 normalTangent = normalize(tbn * vNormalEye);
	vec3 tangentTangent = normalize(tbn * vTangentEye);
	vec3 biTangentTangent = normalize(tbn * vBiTangentEye);
		

	// calculate different angles
	float tld = dot(normalTangent.xyz, normalize(dirLightTangent));
	float thetaLight = degrees(acos(tld));
	
	vec3 dltt = vec3(dirLightTangent.xy, 0.0);
	float pltd = dot(tangentTangent, normalize(dltt));
	float phiLight = degrees(acos(pltd));
	if (dirLightTangent.y < 0.0)
	{
		phiLight = 360.0 - phiLight;
	}
	
	float tvd = dot(normalTangent.xyz, normalize(dirViewTangent));
	float thetaView = degrees(acos(tvd));
	
	vec3 dvtt = vec3(dirViewTangent.xy, 0.0);
	float pvtd = dot(tangentTangent, normalize(dvtt));
	float phiView = degrees(acos(pvtd));
	if (dirViewTangent.y < 0.0)
	{
		phiView = 360.0 - phiView;
	}
	
	// interpolate final color
	vec3 col = calcColor(thetaView, phiView, thetaLight, phiLight).xyz;

	fragColor = vec4(col, 1.0);
}













