#version 420
//
layout(location = 0) in vec3 inPosition;
//
uniform mat4 vp;
//
out vec3 vPosition;

void main()
{
	gl_Position = vp * vec4(inPosition, 1.0);
	vPosition = inPosition;
}
